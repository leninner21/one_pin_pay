import axios from 'axios'


/**
 * 1 - Consulta por cédula
 * 2 - Consulta por código cliente
 * 3 - Consulta por cuenta contrato
 */
const options = {
  selectedOption: "1",
  credential: "1703408854",
};

const TOKEN =
  "8722e2ea52fd44f599d35d1534485d8ea4e4583342f3638f52b6e6b28cc77c97";
const GX_NO_CACHE = "1665065252800";
const AJAX_SECURITY_TOKEN =
  "3019ad49a8dae79b845517a8e509d39a19825d720015a7beca023564e9ce54e285c05756befecfe7f738da2a1ea69a13";
const X_GXAUTH_TOKEN =
  "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJneC1leHAiOiIxNjY2MzYxMjQ0MzUzIiwiZ3gtcGdtIjoiU0FQQ09OU1VMVEFTIn0.Rbkws6iFFyokpxuNaKdSLezx5EOaFpF1M6csgX7S9cM";

const axiosClient = axios.create({
  baseURL: "http://190.120.76.177:8080/consultaplanillas/servlet",
  headers: {
    accept: "*/*",
    "accept-language": "en-US,en;q=0.9,es;q=0.8,fr;q=0.7",
    "content-type": "application/json",
    gxajaxrequest: "1",
    "x-gxauth-token":
      "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJneC1leHAiOiIxNjY2MzYxMjQ0MzUzIiwiZ3gtcGdtIjoiU0FQQ09OU1VMVEFTIn0.Rbkws6iFFyokpxuNaKdSLezx5EOaFpF1M6csgX7S9cM",
  },
});

const getInformationOfElectricLightService =  async ({
  credential,
  selectedOption,
}) => {
  const data = await axiosClient.post(
    "/gob.ec.sapconsultas",
    JSON.stringify({
      "MPage": false,
      "cmpCtx": "",
      "parms": [
        "1703408854",
        {
          "s": "1",
          "v": [
            ["2", "Consulta por Código Cliente*"],
            ["3", "Consulta por Cuenta Contrato"],
            ["1", "Consulta por Cédula o RUC"],
          ],
        },
        1,
        "",
        "",
      ],
      "hsh": [],
      "objClass": "sapconsultas",
      "pkgName": "gob.ec",
      "events": ["'CONSULTAR DATOS'"],
      "grids": {},
    })
  );

  const CONTRACT_NUMBER = data.data.gxValues[1].W0021vGCLIENTESAPCONTRATO_0001

  fetchMainInfoUser(CONTRACT_NUMBER)
};

const fetchMainInfoUser = async (cuentaContrato) => {
  const data = await axios.post(
    'http://186.46.168.153:8080/ServiciosEEQ/ValorUltimaFactura',
    JSON.stringify({
      "cuentaContrato": cuentaContrato,
    })
  )

  console.log(data)
};

getInformationOfElectricLightService(options);